# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import redis

__KEY_BASE_NAME__ = 'items-'

__BASE_ITEMS__ = {
    'carrot': {
        'name': 'carrot',
        'price': 0.72,
        'quantity': 10,
    },
    'orange': {
        'name': 'orange',
        'price': 0.54,
        'quantity': 14,
    },
    'watermelon': {
        'name': 'watermelon',
        'price': 0.32,
        'quantity': 4,
    },
    'tomato': {
        'name': 'tomato',
        'price': 0.58,
        'quantity': 8,
    },
    'pumpkin': {
        'name': 'pumpkin',
        'price': 0.83,
        'quantity': 3,
    },
}


class Database:
    """Class to run a simple database

    Attributes
    ----------
    __redis__conn__ : redis connection

    Static methods
    --------------
    __key_name__(key) : returns the key name

    Methods
    --------------
    __init__(self, load=False) : constructor
    __key__(name) : returns key given a name
    __load__(self) : loads base database
    __exists__(self, name) : checks if an item exists
    __read__(self, name) : reads an item
    __update__(self, name, price, quantity) : updates an item
    create(self, name, price, quantity) : creates a new item
    read(self, name) : reads an item
    update(self, name, price=None, quantity=None) : updates an item
    delete(self, name) : deletes an item
    """

    def __init__(self, load=False):
        """Constructor

        Parameters
        ----------
        load : bool
            Load base items
        """
        self.__redis_conn__ = redis.Redis(host='172.17.0.1')
        if load:
            self.__load__()

    def __key__(name):
        """Returns key given a name

        Parameters
        ----------
        name : str
            Item name
        """
        return '%s%s' % (__KEY_BASE_NAME__, name)

    def __load__(self):
        """Loads base database
        """
        self.__items__ = __BASE_ITEMS__.copy()
        for item, val in __BASE_ITEMS__.items():
            self.__redis_conn__.hmset(Database.__key__(item), val)

    def __exists__(self, name):
        """Checks if an item exists

        Parameters
        ----------
        name : str
            Item name
        """
        self.__redis_conn__.hexists(Database.__key__(name), 'name')

    def __read__(self, name):
        """Reads an item

        Parameters
        ----------
        name : str
            Item name
        """
        item = self.__redis_conn__.hmget(Database.__key__(name), 'name',
                                         'price', 'quantity')
        if item == [None, None, None] or len(item) != 3:
            return None
        return {
            'name': item[0],
            'price': item[1],
            'quantity': item[2],
        }

    def create(self, name, price, quantity):
        """Creates a new item

        Parameters
        ----------
        name : str
            New item name
        price : float
            New item price
        quantity : int
            New item quantity
        """
        if self.__exists__(name):
            raise KeyError("item %s already exists" % name)
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(price, float):
            raise TypeError("expected float for price")
        if not isinstance(quantity, int):
            raise TypeError("expected int for quantity")
        self.__redis_conn__.hmset(
            Database.__key__(name),
            {
                'name': name,
                'price': price,
                'quantity': quantity,
            },
        )
        return self.__read__(name)

    def read(self, name):
        """Reads an item

        Parameters
        ----------
        name : str
            Item name
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        item = self.__read__(name)
        if not item:
            raise KeyError("item %s not found" % name)
        return item

    def update(self, name, price, quantity):
        """Updates an item

        Parameters
        ----------
        name : str
            Item name
        price : float
            Item price
        quantity : int
            Item quantity
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(price, float):
            raise TypeError("expected float for price")
        if not isinstance(quantity, int):
            raise TypeError("expected int for quantity")
        item = self.__read__(name)
        if not item:
            raise KeyError("item %s not found" % name)
        self.__redis_conn__.hmset(
            Database.__key__(name),
            {
                'price': price,
                'quantity': quantity,
            },
        )
        return self.__read__(name)

    def delete(self, name):
        """Deletes an item

        Parameters
        ----------
        name : str
            Item name
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        item = self.__read__(name)
        if not item:
            raise KeyError("item %s not found" % name)
        self.__redis_conn__.hdel(Database.__key__(name), 'name', 'price',
                                 'quantity')
        return item
