# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import consul
from fastapi import FastAPI, HTTPException
import logging
import os
import signal
import sys

from items.database import Database
from items.item import (
    ItemGet,
    ItemAdd,
    ItemUpdate,
    ItemDelete,
)

__logger__ = logging.getLogger('gunicorn.info')

if os.getenv('LOAD_BASE_ITEMS') and (
   os.getenv('LOAD_BASE_ITEMS') == '1' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'Y' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'yes' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'true' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 't'):
    __logger__.info('Loading base items')
    __data__ = Database(load=True)
else:
    __data__ = Database()

__NAME__ = 'items'
__DESCRIPTION = 'Items service'
__VERSION__ = '1.0.0'

# Register to consul
__service_id__ = os.getenv('SERVICE_ID')
__consul_client__ = consul.Consul(host='172.17.0.1')
__consul_client__.agent.service.register(
    __NAME__,
    service_id=__service_id__,
    address='172.17.0.1',
    port=int(os.getenv('SERVICE_PORT')),
    check=consul.Check.http(
        'http://172.17.0.1:%s/' % (os.getenv('SERVICE_PORT')),
        interval='15s',
    ),
)

__data__ = Database()

app = FastAPI()


def __sigint_handler__(signal_received, frame):
    """SIGINT handler

    Parameters
    ----------
    signal_received : str
        Received signal
    frame :
        Signal fram
    """
    __consul_client__.agent.service.deregister(__service_id__)
    sys.exit(0)


# Set SIGINT handler
signal.signal(signal.SIGINT, __sigint_handler__)


@app.get("/")
def root():
    return {
        'name': __NAME__,
        'description': __DESCRIPTION,
        'version': __VERSION__,
    }


@app.get('/items/')
def get_item(item: ItemGet):
    try:
        item_db = __data__.read(item.name)
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except KeyError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return item_db


@app.post('/items/')
async def add_item(item: ItemAdd):
    try:
        item_db = __data__.create(item.name, item.price, item.quantity)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    __logger__.info('Created new item %s' % item.name)
    return item_db


@app.put('/items/')
async def update_item(item: ItemUpdate):
    try:
        item_db = __data__.update(item.name, item.price, item.quantity)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return item_db


@app.delete('/items/')
async def delete_item(item: ItemDelete):
    try:
        item_db = __data__.delete(item.name)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    __logger__.info('removed item %s' % item.name)
    return item_db
