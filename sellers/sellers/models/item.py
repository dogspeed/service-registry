# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests


class Item:
    """Class to define an item

    Attributes
    ----------
    name : item name
    price : item price
    quantity : item quantity

    Methods
    -------
    __init__(self, name, price, quantity) : constructor

    Static methods
    --------------
    query(url, name) : queries an item
    """
    def __init__(self, name, price, quantity):
        """Constructor

        Parameters
        ----------
        name : str
            Item name
        price : float
            Item price
        quantity : int
            Item quantity
        """
        self.name = name
        self.price = price
        self.quantity = quantity

    def query(url, name):
        """Queries an item

        Parameters
        ----------
        url : str
            Items service url
        name : str
            Item name
        """
        r = requests.get('%s/items' % url, json={'name': name})
        if r.status_code == 400:
            raise TypeError(r.json().get('detail'))
        elif r.status_code == 404:
            raise KeyError(r.json().get('detail'))
        elif r.status_code == 500:
            raise Exception(r.json().get('detail'))
        return Item(r.json().get('name'), r.json().get('price'),
                    r.json().get('quantity'))
