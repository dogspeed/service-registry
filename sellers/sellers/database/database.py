# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import redis

from sellers.database.sellers import Sellers
from sellers.database.sells import Sells


class Database:
    """Class to run a simple database

    Attributes
    ----------
    __redis_conn__ : redis connection
    sellers : sellers database
    sells : sells database

    Methods
    -------
    __init__(self, load=False) : constuctor
    """
    def __init__(self, load=False):
        """Constructor

        Parameters
        ----------
        load : bool
            Load base items
        """
        self.__redis_conn__ = redis.Redis(host='172.17.0.1')
        self.sellers = Sellers(self.__redis_conn__, load=load)
        self.sells = Sells(self.__redis_conn__, load=load)
