# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__KEY_BASE_NAME__ = 'sellers-'

__BASE_ITEMS__ = {
    'Homer-Marjorie': {
        'name': 'Homer',
        'surnames': 'Marjorie',
        'birthday': '1977-04-07',
    },
    'Jamie-Leola': {
        'name': 'Jamie',
        'surnames': 'Leola',
        'birthday': '1980-05-29',
    },
    'Shawn-Nyla': {
        'name': 'Shawn',
        'surnames': 'Nyla',
        'birthday': '1981-03-04',
    },
}


class Sellers:
    """Class to run sellers database

    Attributes
    ----------
    __redis_conn__ : redis connection

    Static methods
    --------------
    __init__(self, conn, load=False) : constructor
    __key__(name, surnames=None) : returns key given a name
    __load__(self) : loads base database
    __exists__(self, name, surnames) : checks if a seller exists
    __read__(self, name, surnames) : reads a seller
    create(self, name, surnames, birthday) : creates a new seller
    read(self, name, surnames) : reads a seller
    update(self, name, surnames, birthday=None) : updates a seller
    delete(self, name, surnames) : deletes a seller
    """
    def __init__(self, conn, load=False):
        """Constructor

        Parameters
        ----------
        conn : Redis conn
            Redis open connection
        load : bool
            Load base items
        """
        self.__redis_conn__ = conn
        if load:
            self.__load__()

    def __key__(name, surnames=None):
        """Returns key given a name

        Parameters
        ----------
        name : str
            Seller name
        surnames : str
            Seller surnames
        """
        if surnames:
            return '%s%s-%s' % (__KEY_BASE_NAME__, name, surnames)
        else:
            return '%s%s' % (__KEY_BASE_NAME__, name)

    def __load__(self):
        """Loads base database
        """
        for seller, val in __BASE_ITEMS__.items():
            self.__redis_conn__.hmset(Sellers.__key__(seller), val)

    def __exists__(self, name, surnames):
        """Checks if a seller exists

        Parameters
        ----------
        name : str
            Seller name
        surnames : str
            Seller surnames
        """
        self.__redis_conn__.hexists(Sellers.__key__(name, surnames),
                                    'name')

    def __read__(self, name, surnames):
        """Reads a seller

        Parameters
        ----------
        name : str
            Seller name
        surnames : str
            Seller surnames
        """
        seller = self.__redis_conn__.hmget(Sellers.__key__(name, surnames),
                                           'name', 'surnames', 'birthday')
        if seller == [None, None, None] or len(seller) != 3:
            return None
        return {
            'name': seller[0],
            'surnames': seller[1],
            'birthday': seller[2],
        }

    def create(self, name, surnames, birthday):
        """Creates a new seller

        Parameters
        ----------
        name : str
            New seller name
        surnames : str
            New seller surnames
        birthday : str
            New seller birthday
        """
        if self.__exists__(name, surnames):
            raise KeyError("seller %s %s already exists" % (name, surnames))
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(surnames, str):
            raise TypeError("expected str for surnames")
        if not isinstance(birthday, str):
            raise TypeError("expected str for birthday")
        self.__redis_conn__.hmset(
            Sellers.__key__(name, surnames),
            {
                'name': name,
                'surnames': surnames,
                'birthday': birthday,
            },
        )
        return self.__read__(name, surnames)

    def read(self, name, surnames):
        """Reads a seller

        Parameters
        ----------
        name : str
            Seller name
        surnames : str
            Seller surnames
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(surnames, str):
            raise TypeError("expected str for surnames")
        seller = self.__read__(name, surnames)
        if not seller:
            raise KeyError("seller %s %s not found" % (name, surnames))
        return seller

    def update(self, name, surnames, birthday=None):
        """Updates a seller

        Parameters
        ----------
        name : str
            New seller name
        surnames : str
            New seller surnames
        birthday : str
            New seller birthday
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(surnames, str):
            raise TypeError("expected str for surnames")
        if not isinstance(birthday, str):
            raise TypeError("expected str for birthday")
        seller = self.__read__(name, surnames)
        if not seller:
            raise KeyError("seller %s %s not found" % (name, seller))
        self.__redis_conn__.hmset(
            Sellers.__key__(name, surnames),
            {
                'birthday': birthday,
            },
        )
        return self.__read__(name, surnames)

    def delete(self, name, surnames):
        """Deletes a seller

        Parameters
        ----------
        name : str
            Seller name
        surnames : str
            Seller surnames
        """
        if not isinstance(name, str):
            raise TypeError("expected str for name")
        if not isinstance(surnames, str):
            raise TypeError("expected str for surnames")
        seller = self.__read__(name, surnames)
        if not seller:
            raise KeyError("seller %s %s not found" % (name, surnames))
        self.__redis_conn__.hdel(Sellers.__key__(name, surnames), 'name',
                                 'surnames', 'birthday')
        return seller
