# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
import uuid

from sellers.models import Item

__KEY_BASE_NAME__ = 'sells-'

__BASE_ITEMS__ = {
    '6848ddb2-07bb-11eb-adc1-0242ac120002': {
        'uuid': '6848ddb2-07bb-11eb-adc1-0242ac120002',
        'seller': 'Homer Marjorie',
        'item': 'carrot',
        'quantity': 5,
        'price_unit': 0.68,
        'done_at': time.time(),
    },
    '74004ba4-07bb-11eb-adc1-0242ac120002': {
        'uuid': '74004ba4-07bb-11eb-adc1-0242ac120002',
        'seller': 'Homer Marjorie',
        'item': 'tomato',
        'quantity': 1,
        'price_unit': 0.62,
        'done_at': time.time(),
    },
    '79c47cfe-07bb-11eb-adc1-0242ac120002': {
        'uuid': '79c47cfe-07bb-11eb-adc1-0242ac120002',
        'seller': 'Homer Marjorie',
        'item': 'orange',
        'quantity': 2,
        'price_unit': 0.53,
        'done_at': time.time(),
    },
}


class Sells:
    """Class to run sells database

    Attributes
    ----------
    __redis_conn__ : redis connection

    Static methods
    --------------
    __init__(self, conn, load=False) : constructor
    __key__(uuid_key) : returns key given an uuid
    __load__(self) : loads base database
    __exists__(self, uuid_key) : checks if a sell exists
    __read__(self, uuid_key) : reads a sell
    create(self, seller, item, quantity) : creates a new sell
    read(self, uuid_key) : reads a seller
    """
    def __init__(self, conn, load=False):
        """Constructor

        Parameters
        ----------
        conn : Redis conn
            Redis open connection
        load : bool
            Load base items
        """
        self.__redis_conn__ = conn
        if load:
            self.__load__()

    def __key__(uuid_key):
        """Returns key given an uuid

        Parameters
        ----------
        uuid_key : str
            Sell UUID
        """
        return '%s%s' % (__KEY_BASE_NAME__, uuid_key)

    def __load__(self):
        """Loads base database
        """
        for sell, val in __BASE_ITEMS__.items():
            self.__redis_conn__.hmset(Sells.__key__(sell), val)

    def __exists__(self, uuid_key):
        """Checks if a sell exists

        Parameters
        ----------
        uuid_key : str
            Sell UUID
        """
        self.__redis_conn__.hexists(Sells.__key__(uuid_key), 'uuid')

    def __read__(self, uuid_key):
        """Reads a sell

        Parameters
        ----------
        uuid_key : str
            Sell UUID
        """
        sell = self.__redis_conn__.hmget(Sells.__key__(uuid_key), 'uuid',
                                         'seller', 'item', 'quantity',
                                         'price_unit', 'done_at')
        if sell == [None, None, None, None, None, None] or len(sell) != 6:
            return None
        return {
            'uuid': sell[0],
            'seller': sell[1],
            'item': sell[2],
            'quantity': sell[3],
            'price_unit': sell[4],
            'done_at': sell[5],
        }

    def create(self, seller, item, quantity):
        """Creates a new seller

        Parameters
        ----------
        seller : str
            New sell seller
        item : Item
            New sell item
        quantity : int
            New sell quantity
        """
        if not isinstance(seller, str):
            raise TypeError("expected str for seller")
        if not isinstance(item, Item):
            raise TypeError("expected Item for item")
        if not isinstance(quantity, int):
            raise TypeError("expected int for quantity")
        uuid_key = str(uuid.uuid1())
        self.__redis_conn__.hmset(
            Sells.__key__(uuid_key),
            {
                'uuid': uuid_key,
                'seller': seller,
                'item': item.name,
                'quantity': quantity,
                'price_unit': item.price,
                'done_at': time.time()
            },
        )
        return self.__read__(uuid_key)

    def read(self, uuid_key):
        """Reads a seller

        Parameters
        ----------
        uuid_key : str
            Sell UUID
        """
        if not isinstance(uuid_key, str):
            raise TypeError("expected str for uuid_key")
        sell = self.__read__(uuid_key)
        if not sell:
            raise KeyError("sell %s not found" % uuid_key)
        return sell
