# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import Optional
from pydantic import BaseModel


class SellerGet(BaseModel):
    """Class to define a seller to get
    """
    name: str
    surnames: str


class SellerAdd(BaseModel):
    """Class to define a seller to add
    """
    name: str
    surnames: str
    birthday: str


class SellerUpdate(BaseModel):
    """Class to define a seller to update
    """
    name: str
    surnames: str
    birthday: Optional[str] = None


class SellerDelete(BaseModel):
    """Class to define a seller to delete
    """
    name: str
    surnames: str
