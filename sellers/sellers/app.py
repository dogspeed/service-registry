# service registry
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import consul
from fastapi import FastAPI, HTTPException
import logging
import os
import signal
import sys

from sellers.database import Database
from sellers.models import Item
from sellers.sell import (
    SellGet,
    SellAdd,
)
from sellers.seller import (
    SellerGet,
    SellerAdd,
    SellerUpdate,
    SellerDelete,
)

__logger__ = logging.getLogger('gunicorn.info')

if os.getenv('LOAD_BASE_ITEMS') and (
   os.getenv('LOAD_BASE_ITEMS') == '1' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'Y' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'yes' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 'true' or
   os.getenv('LOAD_BASE_ITEMS').lower() == 't'):
    __logger__.info('Loading base sellers')
    __data__ = Database(load=True)
else:
    __data__ = Database()

__NAME__ = 'sellers'
__DESCRIPTION = 'Sellers service'
__VERSION__ = '1.0.0'

# Register to consul
__service_id__ = os.getenv('SERVICE_ID')
__consul_client__ = consul.Consul(host='172.17.0.1')
__consul_client__.agent.service.register(
    __NAME__,
    service_id=__service_id__,
    address='172.17.0.1',
    port=int(os.getenv('SERVICE_PORT')),
    check=consul.Check.http(
        'http://172.17.0.1:%s/' % (os.getenv('SERVICE_PORT')),
        interval='15s',
    ),
)

app = FastAPI()


def __sigint_handler__(signal_received, frame):
    """SIGINT handler

    Parameters
    ----------
    signal_received : str
        Received signal
    frame :
        Signal fram
    """
    __consul_client__.agent.service.deregister(__service_id__)
    sys.exit(0)


# Set SIGINT handler
signal.signal(signal.SIGINT, __sigint_handler__)


@app.get("/")
def root():
    return {
        'name': __NAME__,
        'description': __DESCRIPTION,
        'version': __VERSION__,
    }


@app.get('/sellers/')
def get_seller(seller: SellerGet):
    try:
        seller_db = __data__.sellers.read(seller.name, seller.surnames)
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except KeyError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return seller_db


@app.post('/sellers/')
async def add_seller(seller: SellerAdd):
    try:
        seller_db = __data__.sellers.create(seller.name, seller.surnames,
                                            seller.birthday)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    __logger__.info('Created new seller %s %s' % (seller.name,
                                                  seller.surnames))
    return seller_db


@app.put('/sellers/')
async def update_seller(seller: SellerUpdate):
    try:
        seller_db = __data__.sellers.update(seller.name, seller.surnames,
                                            seller.birthday)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return seller_db


@app.delete('/sellers/')
async def delete_seller(seller: SellerDelete):
    try:
        seller_db = __data__.sellers.delete(seller.name, seller.surnames)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    __logger__.info('removed seller %s %s' % (seller.name, seller.surnames))
    return seller_db


@app.get('/sells/')
def get_sell(sell: SellGet):
    try:
        sell_db = __data__.sells.read(sell.uuid)
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except KeyError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return sell_db


@app.post('/sells/')
async def add_sell(sell: SellAdd):
    # Search for item on item services
    try:
        # TODO: check consul for service items
        item = Item.query('http://172.17.0.1:8001', sell.item)
        sell_db = __data__.sells.create(sell.seller, item, sell.quantity)
    except KeyError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except TypeError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    __logger__.info('Created new sell %s' % sell_db.get('uuid'))
    return sell_db
